package org.tio.examples.im.server.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tio.core.ChannelContext;
import org.tio.core.TioConfig;
import org.tio.core.Tio;
import org.tio.examples.im.common.ImPacket;
import org.tio.examples.im.common.ImSessionContext;
import org.tio.examples.im.common.http.HttpRequestPacket;
import org.tio.examples.im.common.http.websocket.WebsocketPacket;
import org.tio.examples.im.common.packets.ChatRespBody;
import org.tio.examples.im.common.packets.ChatType;
import org.tio.examples.im.common.packets.Client;
import org.tio.examples.im.common.packets.Command;
import org.tio.examples.im.common.packets.JoinGroupNotifyRespBody;
import org.tio.examples.im.common.packets.JoinGroupReqBody;
import org.tio.examples.im.common.packets.JoinGroupRespBody;
import org.tio.examples.im.common.packets.JoinGroupResult;
import org.tio.examples.im.common.utils.ImUtils;
import org.tio.utils.SystemTimer;

import cn.hutool.core.util.StrUtil;

/**
 *
 *
 * @author tanyaowu
 *
 */
public class JoinReqHandler implements ImBsHandlerIntf {
	private static Logger log = LoggerFactory.getLogger(JoinReqHandler.class);

	//	public static String[] imgs = new String[] {
	//			//						"http://p2.so.qhmsg.com/t016a799588a1a805b7.jpg"
	//			//						,"http://p1.so.qhimgs1.com/t014a25fa412c88726d.jpg"
	//			//						//章天泽
	//			//						,"http://p0.so.qhmsg.com/bdr/_240_/t015283ba5bcf7de205.jpg"
	//			//						,"http://p2.so.qhimgs1.com/bdr/_240_/t01705d6eaa265f7852.jpg"
	//			//						,"http://p0.so.qhmsg.com/bdr/_240_/t01ac699b2cc8653ee5.jpg"
	//			//						,"http://p2.so.qhmsg.com/bdr/_240_/t010c6e25cc7ba89cb7.jpg"
	//			//						,"http://p0.so.qhmsg.com/bdr/_240_/t01dede1c8875fdd895.jpg"
	//			//						,"http://p0.so.qhmsg.com/bdr/_240_/t014531e2ff627d7f45.jpg"
	//			//						,"http://p2.so.qhimgs1.com/bdr/_240_/t017e0fe253e725752e.jpg"
	//			//						,"http://p1.so.qhimgs1.com/bdr/_240_/t01ef258e81cfdef111.jpg"
	//			//						,"http://p2.so.qhimgs1.com/bdr/_240_/t0101fec1fbe7be779e.png"
	//			//						,"http://p0.so.qhmsg.com/bdr/_240_/t01bdfef08b250e6f9f.jpg"
	//			//
	//			//
	//			//
	//			//						//林依晨
	//			//						,"http://p3.so.qhmsg.com/bdr/_240_/t011cf9fa4cee87e408.jpg"
	//			//						,"http://p3.so.qhimgs1.com/bdr/_240_/t0155271eb256935093.jpg"
	//			//						,"http://p0.so.qhimgs1.com/bdr/_240_/t01d2c66efe4c2ec0b0.jpg"
	//			//						//景甜
	//			//						,"http://p4.so.qhimgs1.com/bdr/_240_/t011ae520c7e55923ea.jpg"
	//			//						,"http://p2.so.qhmsg.com/bdr/_240_/t01a4cd21a3a5badca7.jpg"
	//			//						,"http://p0.so.qhmsg.com/bdr/_240_/t01db5bfeeca64ae04f.jpg"
	//			//						,"http://p4.so.qhmsg.com/bdr/_240_/t0119179904fd21d499.jpg"
	//
	//			//唐艺昕
	//			"http://p4.so.qhimgs1.com/bdr/_240_/t01c883fa022ed309a4.jpg", "http://p4.so.qhimgs1.com/bdr/_240_/t01962fefe662849b5a.jpg",
	//			"http://p4.so.qhimgs1.com/bdr/_240_/t01ee3d8ed4ee60d73a.jpg", "http://p0.so.qhimgs1.com/bdr/_240_/t0114e9bd549f35cc5c.jpg",
	//			"http://p2.so.qhmsg.com/bdr/_240_/t0109da4f85f0e12c97.jpg", "http://p4.so.qhmsg.com/bdr/_240_/t010a0536093d42b969.jpg",
	//			"http://p4.so.qhimgs1.com/bdr/_240_/t01fdb65bb57cb994fa.jpg", "http://p5.so.qhimgs1.com/bdr/_240_/t0179d8ea81a089419b.jpg",
	//			"http://p1.so.qhimgs1.com/bdr/_240_/t0160a1d98168b1c991.jpg", "http://p4.so.qhimgs1.com/bdr/_240_/t01b46d864dff7df59a.jpg",
	//			"http://p0.so.qhimgs1.com/bdr/_240_/t0128c84c151ed704cc.jpg", "http://p1.so.qhmsg.com/bdr/_240_/t0149015239458be2d6.jpg",
	//			"http://p2.so.qhimgs1.com/bdr/_240_/t01f40b589b70444f82.jpg", "http://p0.so.qhmsg.com/bdr/_240_/t012241532f024cb585.jpg",
	//			"http://p4.so.qhimgs1.com/bdr/_240_/t01859834679a1f5fda.jpg", "http://p3.so.qhmsg.com/bdr/_240_/t01c4b9db98c6f91d18.jpg",
	//			"http://p2.so.qhimgs1.com/bdr/_240_/t01106cd4592d9e7a3e.jpg", "http://p1.so.qhmsg.com/bdr/_240_/t01b4c1fd7832d49876.jpg",
	//			"http://p5.so.qhimgs1.com/bdr/_240_/t01da6afaa651507a4b.jpg", "http://p3.so.qhmsg.com/bdr/_240_/t011ff4f173d1f08298.jpg",
	//			"http://p4.so.qhmsg.com/bdr/_240_/t016df7ce4c996c0359.jpg", "http://p5.so.qhimgs1.com/bdr/_240_/t013601d1899e9684cb.jpg",
	//			"http://p4.so.qhmsg.com/bdr/_240_/t0100974e075fe63de9.jpg", "http://p2.so.qhmsg.com/bdr/_240_/t01d879b6f4ee1a5fa7.jpg",
	//			"http://p3.so.qhimgs1.com/bdr/_240_/t01f39f3db828dcf6d3.jpg", "http://p4.so.qhmsg.com/bdr/_240_/t01e74c4c63f4b51959.jpg",
	//			"http://p1.so.qhimgs1.com/bdr/_240_/t01fdeed0b93fec0c9d.png", "http://p0.so.qhimgs1.com/bdr/_240_/t018b04bd0470ff6aa0.jpg",
	//			"http://p2.so.qhimgs1.com/bdr/_240_/t01bd239a83ddd23c7e.jpg", "http://p1.so.qhimgs1.com/bdr/_240_/t01ad50753a372de29d.jpg",
	//			"http://p1.so.qhimgs1.com/bdr/_240_/t0189b7462256934fad.jpg", "http://p0.so.qhmsg.com/bdr/_240_/t01da640ad404f26acf.png"
	//
	//	};

	@Override
	public Object handler(ImPacket packet, ChannelContext channelContext) throws Exception {
		if (packet.getBody() == null) {
			throw new Exception("body is null");
		}

		JoinGroupReqBody reqBody = JoinGroupReqBody.parseFrom(packet.getBody());

		String group = reqBody.getGroup();
		if (StrUtil.isBlank(group)) {
			log.error("group is null,{}", channelContext);
			Tio.close(channelContext, "group is null when join group");
			return null;
		}
		//		TioConfig tioConfig = channelContext.tioConfig;

		ImSessionContext imSessionContext = (ImSessionContext) channelContext.getAttribute();
		HttpRequestPacket httpHandshakePacket = imSessionContext.getHttpHandshakePacket();
		Tio.bindGroup(channelContext, group);

		//回一条消息，告诉对方进群结果
		JoinGroupResult joinGroupResult = JoinGroupResult.JOIN_GROUP_RESULT_OK;
		JoinGroupRespBody joinRespBody = JoinGroupRespBody.newBuilder().setResult(joinGroupResult).setGroup(group).build();
		WebsocketPacket respPacket = new WebsocketPacket(Command.COMMAND_JOIN_GROUP_RESP, joinRespBody.toByteArray());
		Tio.send(channelContext, respPacket);

		//发进群通知  COMMAND_JOIN_GROUP_NOTIFY_RESP
		JoinGroupNotifyRespBody joinGroupNotifyRespBody = JoinGroupNotifyRespBody.newBuilder().setGroup(group).setClient(imSessionContext.getClient()).build();
		WebsocketPacket respPacket2 = new WebsocketPacket(Command.COMMAND_JOIN_GROUP_NOTIFY_RESP, joinGroupNotifyRespBody.toByteArray());
		Tio.sendToGroup(channelContext.tioConfig, group, respPacket2);
		//		respPacket2.setBody(body);


		Client currClient = imSessionContext.getClient();
		String nick = currClient.getUser().getNick();
		String region = imSessionContext.getDataBlock().getRegion();

		String formatedUserAgent = ImUtils.formatUserAgent(channelContext);

		//		String imgsrc = "http://images.rednet.cn/articleimage/2013/01/23/1403536948.jpg";
		//		String href = "http://mp.weixin.qq.com/s/RSi8Au0n7UrlebVVYLGFGw";
		//		String title = "";
		String content = "";
	
		String text = content;

		ChatRespBody.Builder builder = ChatRespBody.newBuilder();
		builder.setType(ChatType.CHAT_TYPE_PUBLIC);
		builder.setText(text);
		builder.setFromClient(org.tio.examples.im.service.UserService.sysClient);
		//		builder.setGroup(group);
		builder.setTime(SystemTimer.currTime);
		TioConfig tioConfig = channelContext.tioConfig;
		builder.setId(tioConfig.getTioUuid().uuid());
		ChatRespBody chatRespBody = builder.build();
		WebsocketPacket respPacket1 = new WebsocketPacket(Command.COMMAND_CHAT_RESP, chatRespBody.toByteArray());
		//		Tio.send(channelContext, respPacket1);

		return null;
	}
}
