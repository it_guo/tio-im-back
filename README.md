# tio-im-back

#### 介绍
两年前用t-io写的一个im实验品，大家熟知的j-im就是基于这个代码改造的，本工程的数据格式是protobuf的，j-im后来改成json的了，另外本人已经把t-io升级到最新版本了：3.5.7.v20191115-RELEASE

#### 在线访问
http://tx.t-io.org
[![输入图片说明](https://images.gitee.com/uploads/images/2019/1206/170910_a4803ab7_355738.png "QQ图片20191206170827.png")](http://tx.t-io.org)


#### 安装教程

1.  page目录是页面文件，需要运行在http服务器下(演示环境运行在jetty下)，里面有一个ws的地址，您需要修改一下
2.  其它目录，是标准的maven工程，不一一说明
3.  IM启动类在：org.tio.examples.im.server.ImServerStarter

#### 说明

1.  一直有用户私下让我开源这个，只是一直没时间
2.  我本人仅仅是开源这个代码，但并不会继续维护，请事先知道一下，到时候省得浪费您的时间


#### 参与贡献

可以关注一下t-io：https://gitee.com/tywo45/t-io

