// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import 'core-js/fn/promise'
// import 'core-js/fn/array/find'
// import 'core-js/fn/array/find-index'
// import 'core-js/fn/array/includes'

import 'core-js'


import Vue from 'vue'
import App from './App'




import './assets/css/tioim.css'; // 使用 CSS
import './assets/css/3d.css'; // 使用 CSS


//动画: https://github.com/radical-dreamers/animated-vue
// npm install --save animated-vue
// import AnimatedVue from 'animated-vue';
//cnpm install --save animate.css
import 'animate.css/animate.min.css';
// Vue.use(AnimatedVue);


require('iconoo');

//iview
import iView from 'iview';
import 'iview/dist/styles/iview.css'; // 使用 CSS
Vue.use(iView);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    template: '<App/>',
    components: { App }
})

// new Vue({
//     el: '#userlist',
//     //template: '<App/>',
//     components: { userlist }
// })

// new Vue({
//     el: '#chat',
//     //template: '<App/>',
//     components: { chat }
// })