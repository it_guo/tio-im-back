var Vue = require("vue");
var Protobuf = require("google-protobuf");
import ArrayUtil from './ArrayUtil.js';
import ImProto from '../chat_pb.js';

class Tio {
    /**
     * 
     * @param {*} command  proto.Command.COMMAND_CHAT_RESP
     * @param {*} bodyObj new proto.Client()
     * @param {*} ws 
     */
    static send(command, bodyObj, ws) {
        let arrayBuffer = ArrayUtil.toArrayBuffer(command, bodyObj);
        if (ws) {
            ws.send(arrayBuffer);
        }
    };

    /**
     * 是否是一个function
     * @param {*} test 
     */
    static isFun(test) {
        return typeof test == 'function';
    };

    /**
     * 是否是一个object
     * @param {*} test 
     */
    static isObject(test) {
        return typeof test == 'object';
    };

    /**
     * 把首字母大写
     * @param {*} str 
     */
    static firstToUpperCase(str) {
        return str.replace(/(\w)/, function(v) { return v.toUpperCase() });
    };
    /**
     * 把首字母小写
     * @param {*} str 
     */
    static firstToLowerCase(str) {
        return str.replace(/(\w)/, function(v) { return v.toLowerCase() });
    };

    /**
     * 对uint8Array进行protobuf协议解码
     * @param {*} Proto  形如: ImProto.JoinGroupNotifyRespBody
     * @param {*} uint8Array bytes
     */
    static proto(Proto, uint8Array) {
        let ret = Proto.deserializeBinary(uint8Array);
        Tio.addProp(ret);
        return ret;
    };

    /**
     * 为有getXxx()方法却没有xxx属性的对象添加xxx属性
     * @param {*} obj 
     */
    static addProp(obj) {
        if (!obj) {
            return;
        }
        if (obj instanceof Array) {
            // console.log("这货是数组", obj);
            for (let i = 0; i < obj.length; i++) {
                let e = obj[i];
                Tio.addProp(e);
            }
            return;
        }
        for (let key in obj) {
            try {
                let val = obj[key];
                if (key.startsWith("get") && Tio.isFun(val) && !obj.hasOwnProperty(key)) {
                    let propName = Tio.firstToLowerCase(key.substring(3));
                    // console.log(propName);
                    let propV = val.call(obj);
                    obj[propName] = propV;
                    if (Tio.isObject(propV)) {
                        Tio.addProp(propV);
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    }
}

export default Tio;