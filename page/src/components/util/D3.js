class D3 {
    /**
     * 组合数组到targetArray
     * @param {*} targetArray 
     * @param {*} allArr [array1, array2... arrayn]
     */
    static data() {
        let xx = [
            // "科比1，动画": 
            {
                top: "http://p1.so.qhmsg.com/bdr/_240_/t0129a63d9fb3856af6.gif",
                bottom: "http://p1.so.qhimgs1.com/bdr/_240_/t018cbe768863a5b5dd.gif",
                left: "http://p3.so.qhmsg.com/bdr/_240_/t0189a238f624cea118.gif",
                right: "http://p3.so.qhmsg.com/bdr/_240_/t01ef184b27a937f0e8.gif",
                front: "http://p0.so.qhmsg.com/bdr/_240_/t011270b64e1bc2964f.gif",
                back: "http://p2.so.qhimgs1.com/bdr/_240_/t01120adb81dba304b2.gif"
            },
            // "红薯": 
            {
                top: "https://static.oschina.net/uploads/cover/2686220_EQmEQ_bi.jpg",
                bottom: "http://u.hizh.cn/u/cms/www/201609/12111538tmk4.jpg",
                left: "http://upload.idcquan.com/2017/0424/1493012808617.jpg",
                right: "http://static.oschina.net/uploads/space/2016/0321/164917_XDmf_2652078.jpg",
                front: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1498213330069&di=ac8b5410b58e4b54a9f599985b78beb2&imgtype=0&src=http%3A%2F%2Fstatic.oschina.net%2Fuploads%2Fspace%2F2014%2F1223%2F064541_y4BG_2012764.jpg",
                back: "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1498213348202&di=3d04fde25a497ea0a135b4015f6bb45e&imgtype=0&src=http%3A%2F%2Fwww.oschina.net%2Fuploads%2Fspace%2F2012%2F0702%2F221518_n4YQ_28.jpg"
            },
            // "杨幂": 
            {
                top: "http://img.mingxing.com/upload/attach/2017/02-24/305975-eOyVss.jpg",
                bottom: "http://img.mingxing.com/upload/attach/2017/02-24/305975-6tnXcp.jpg",
                left: "http://img.mingxing.com/upload/attach/2017/02-24/305975-2Rejru.jpg",
                right: "http://img.mingxing.com/upload/attach/2017/02-24/305975-0ieXkw.jpg",
                front: "http://img.mingxing.com/upload/attach/2017/02-24/305974-EAKoqx.jpg",
                back: "http://img.mingxing.com/upload/attach/2017/02-24/305975-i9Qcmu.jpg"
            }, 
            // "马云": 
            {
                top: "http://p4.so.qhimgs1.com/bdr/_240_/t010b9f467025472cea.jpg",
                bottom: "http://minimg.hexun.com/i3.hexunimg.cn/2013-11-21/159890526_c598x373.jpg",
                left: "http://i1.sinaimg.cn/IT/2010/1110/20101110172056.jpg",
                right: "http://img1.cache.netease.com/catchpic/A/A0/A0D7D10B4CAE9FC4FB81949AEB5B4804.png",
                front: "http://img.zjol.com.cn/pic/0/05/97/59/5975902_550244.jpg",
                back: "http://www.cnwnews.com/uploads/allimg/090120/1121380.jpg"
            }, 
            // "科比2": 
            {
                top: "http://p1.so.qhimgs1.com/bdr/_240_/t0124b383016165c591.jpg",
                bottom: "http://p2.so.qhimgs1.com/bdr/_240_/t01f3d0310aba407afe.jpg",
                left: "http://p0.so.qhimgs1.com/bdr/_240_/t0194c356d5cf1f1a6c.jpg",
                right: "http://p5.so.qhimgs1.com/bdr/_240_/t0182e0f89629d5f72b.jpg",
                front: "http://p0.so.qhimgs1.com/bdr/_240_/t01a1802eec83ea87f0.jpg",
                back: "http://p3.so.qhimgs1.com/bdr/_240_/t01d43f48f760f35a53.gif"
            }
            // , "马云": {
            //     top: "",
            //     bottom: "",
            //     left: "",
            //     right: "",
            //     front: "",
            //     back: ""
            // }
        ];
        return xx;
    }
}





export default D3;