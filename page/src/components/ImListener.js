//ArrayBuffer等参考资料：http://javascript.ruanyifeng.com/stdlib/arraybuffer.html

var Vue = require("vue");
var Protobuf = require("google-protobuf");
// var Bytebuffer = require("bytebuffer");
import ArrayUtil from './util/ArrayUtil.js';
import Tio from './util/Tio.js';
import ImProto from './chat_pb.js';
import ImHandler from './ImHandler.js';

//console.log("Protobuf");
//console.log(Protobuf);
// //console.log("Bytebuffer");
// //console.log(Bytebuffer);
//console.log("ImProto");
//console.log(ImProto);
//console.log("ImProto.AuthReqBody");
//console.log(ImProto.AuthReqBody);
//console.log("proto.AuthReqBody === ImProto.AuthReqBody");
//console.log(proto.AuthReqBody === ImProto.AuthReqBody);

// //编码, ws接收到的对象是ArrayBuffer，会用到 concat()
// let client = new proto.Client();
// client.setId(232323);
// client.setIp("127.0.0.1");
// let arrayBuffer = ArrayUtil.toArrayBuffer(proto.Command.COMMAND_AUTH_RESP, client);
// //console.log("arrayBuffer", arrayBuffer);


// //解码, ws发送的对象是ArrayBuffer， 可能会用到shift() 方法用于把数组的第一个元素从其中删除， 并返回第一个元素的值。
// let commandByte = new Uint8Array(arrayBuffer, 0, 1);
// let bodyBytes = new Uint8Array(arrayBuffer, 1);
// //console.log("commandByte", commandByte, "bodyBytes", bodyBytes);
// let client2 = proto.Client.deserializeBinary(bodyBytes);
// //console.log(client, client2);

// ImHandler.handler(commandByte, bodyBytes, null);

//ImHandler.parseCommand(9);



class ImListener {
    constructor(im, imHandler) {
        this.im = im;
        this.imHandler = imHandler;
    }

    onopen(event, ws) {
        //发鉴权消息
        let authReqBody = new ImProto.AuthReqBody();

        //console.log(event, ws);
        let GeolocationListener = function() {
            this.complete = function(data) {
                // let str = ['定位成功'];
                // str.push('经度：' + data.position.getLng());
                // str.push('纬度：' + data.position.getLat());
                // if (data.accuracy) {
                //     str.push('精度：' + data.accuracy + ' 米');
                // }//如为IP精确定位结果则没有精度信息
                // str.push('是否经过偏移：' + (data.isConverted ? '是' : '否'));

                console.log(data);
                let geolocation = new ImProto.Geolocation();
                geolocation.setLng(data.position.getLng());
                geolocation.setLat(data.position.getLat());
                authReqBody.setGeolocation(geolocation);

                let address = new ImProto.Address();
                let ad = data.addressComponent;
                address.setFormattedaddress(data.formattedAddress);
                address.setAdcode(ad.adcode);
                address.setProvince(ad.province);
                address.setCity(ad.city);
                address.setCitycode(ad.citycode);
                address.setDistrict(ad.district);
                address.setTownship(ad.township);
                address.setStreet(ad.street);
                address.setStreetnumber(ad.streetNumber);
                authReqBody.setAddress(address);

                console.log(authReqBody);

                Tio.send(ImProto.Command.COMMAND_AUTH_REQ, authReqBody, ws);

                // document.getElementById('tip').innerHTML = str.join('<br>');
            };
            this.error = function(data) {
                console.error("定位失败", data);
                Tio.send(ImProto.Command.COMMAND_AUTH_REQ, authReqBody, ws);
            }
        };




        //console.log(authReqBody);
        authReqBody.setDeviceid("deviceId--888888888888");
        authReqBody.setSeq(1);
        authReqBody.setDevicetype(ImProto.DeviceType.DEVICE_TYPE_PC);
        authReqBody.setDeviceinfo("chrome");
        authReqBody.setToken("token");

        try {
            let mapObj = new AMap.Map('im_stat_wrap', {
                resizeEnable: true,
                // zoom: 10,
                // center: [118.4253323, 31.80452471]
            });
            console.log(mapObj);

            mapObj.plugin('AMap.Geolocation', function() {
                let geolocation = new AMap.Geolocation({
                    enableHighAccuracy: true, //是否使用高精度定位，默认:true
                    noIpLocate: 0,
                    noGeoLocation: 0,
                    // GeoLocationFirst: true, //默认为false，设置为true的时候可以调整PC端为优先使用浏览器定位，失败后使用IP定位
                    showMarker: true,
                    timeout: 10000, //超过10秒后停止定位，默认：无穷大
                    buttonOffset: new AMap.Pixel(10, 20), //定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
                    zoomToAccuracy: true, //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
                    buttonPosition: 'RB'
                });
                mapObj.addControl(geolocation);
                geolocation.getCurrentPosition();
                let geolocationListener = new GeolocationListener();
                AMap.event.addListener(geolocation, 'complete', geolocationListener.complete); //返回定位信息
                AMap.event.addListener(geolocation, 'error', geolocationListener.error); //返回定位出错信息
            });
        } catch (e) {
            Tio.send(ImProto.Command.COMMAND_AUTH_REQ, authReqBody, ws);
        }


    }
    onmessage(event, ws) {
        var arrayBuffer = event.data;
        //console.log(this, this.imHandler, event);
        this.imHandler.decode(arrayBuffer, ws, event);
    }
    onclose(event, ws) {

    }

    onerror(event, ws) {

    }


}
export default ImListener;